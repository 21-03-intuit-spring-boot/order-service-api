package com.classpath.orderserviceapi.util;

import com.classpath.orderserviceapi.model.LineItem;
import com.classpath.orderserviceapi.model.Order;
import com.classpath.orderserviceapi.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Slf4j
@Component
@RequiredArgsConstructor
@Profile("dev")
public class BootstrapAppData {

    private final OrderRepository orderRepository;
    private final Faker faker = new Faker();

    @EventListener(ApplicationReadyEvent.class)
    public void bootstrapAppData(ApplicationReadyEvent event){
        log.info("======== Application is getting initialized with bootstrap data =========");

        IntStream.range(0, 20_000).forEach(index -> {
            Order order = Order.builder()
                    .customerName(faker.name().fullName())
                    .price(faker.number().randomDouble(2, 5_000, 10_000))
                    .date(faker.date().past(8, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                    .email(faker.internet().emailAddress())
                    .build();
            IntStream.range(0, faker.number().numberBetween(2,5))
                    .forEach(value -> {
                        LineItem lineItem = LineItem.builder()
                                                        .name(faker.commerce().productName())
                                                        .price(faker.number().randomDouble(2, 1000, 2000))
                                                        .qty(faker.number().numberBetween(2, 4))
                                                        .build();
                        order.addLineItem(lineItem);
                    });
                this.orderRepository.save(order);
        });
        log.info("======== Application is getting initialized with bootstrap data =========");
    }
}
