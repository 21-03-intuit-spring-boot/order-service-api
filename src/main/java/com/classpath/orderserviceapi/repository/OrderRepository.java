package com.classpath.orderserviceapi.repository;

import com.classpath.orderserviceapi.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findByPriceBetween(double min, double max);

    List<Order> findByPriceLessThan(double max);

}
