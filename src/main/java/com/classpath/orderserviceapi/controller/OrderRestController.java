package com.classpath.orderserviceapi.controller;

import com.classpath.orderserviceapi.model.Order;
import com.classpath.orderserviceapi.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/orders")
public class OrderRestController {

    private final OrderService orderService;

    public OrderRestController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Order saveOrder(@RequestBody @Valid Order order){
        order.getLineItems().forEach(lineItem -> lineItem.setOrder(order));
        return this.orderService.saveOrder(order);
    }

    @GetMapping
    public Map<String, Object> fetchOrders(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "10") int size,
            @RequestParam(name = "sort", required = false, defaultValue = "asc") String direction,
            @RequestParam(name = "field", required = false, defaultValue = "customerName") String property){
        return this.orderService.fetchAllOrders(page, size, direction, property);
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable long id){
        return this.orderService.fetchOrderById(id);
    }

    @GetMapping("/price")
    public List<Order> fetchOrderByPriceRange(@RequestParam(name = "min") int min, @RequestParam("max") int max){
        return this.orderService.fetchOrderByPriceRange(min, max);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderById(@PathVariable("id") long id){
        this.orderService.deleteOrderById(id);
    }
}
