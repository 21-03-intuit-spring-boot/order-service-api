package com.classpath.orderserviceapi.config;

import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    @ConditionalOnProperty(
            prefix = "app",
            value = "loadUserBean",
            havingValue = "false",
            matchIfMissing = true)
    public User userBean (){
        return new User();
    }

    @Bean
    @ConditionalOnBean(name = "userBean")
    public User userBeanOnBeanCondition (){
        return new User();
    }

    @Bean
    @ConditionalOnMissingBean(name = "userBean")
    public User userBeanOnMissingBeanCondition (){
        return new User();
    }

}

class User {

}
