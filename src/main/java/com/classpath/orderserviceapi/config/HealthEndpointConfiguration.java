package com.classpath.orderserviceapi.config;


import com.classpath.orderserviceapi.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class DBHealthCheckEndpoint implements HealthIndicator{
    private final OrderRepository orderRepository;
    @Override
    public Health health() {
        try {
            long count = this.orderRepository.count();
            if (count < 0)
                return Health.up().withDetail("DB service", "DB service is up").build();
            else
                return Health.down().withDetail("DB service", "DB service is down").build();

        }catch (Exception exception){
            return Health.down().withDetail("DB service", "DB service is down").build();
        }
    }
}
@Component
@RequiredArgsConstructor
class PaymentGatewayHealthCheckEndpoint implements HealthIndicator{

    @Override
    public Health health() {
        return Health.up().withDetail("Payment service", "Payment service is up").build();

    }
}