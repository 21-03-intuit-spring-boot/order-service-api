package com.classpath.orderserviceapi.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ApplicationConfiguration implements CommandLineRunner {

    private final ApplicationContext applicationContext;

    public ApplicationConfiguration(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void run(String... args) throws Exception {

        String[] beanDefinitionNames = this.applicationContext.getBeanDefinitionNames();
        List.of(beanDefinitionNames)
                .stream()
                .filter(bean -> bean.startsWith("user"))
                .forEach(beanName -> System.out.println("Bean :: "+ beanName));

    }
}
