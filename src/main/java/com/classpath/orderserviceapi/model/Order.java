package com.classpath.orderserviceapi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long orderId;

    @Min(value = 2000, message = "Min order price should be 2000 INR")
    @Max(value = 10000, message = "Max order price cannot be greater than 10000 INR")
    private double price;

    @PastOrPresent(message = "Order date cannot be in the past")
    private LocalDate date;

    @NotEmpty(message = "cusomer name cannot be blank")
    private String customerName;

    @Email(message = "email is in invalid format")
    private String email;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<LineItem> lineItems;

    //scaffolding code
    //birectional mapping
    public void addLineItem(LineItem lineItem){
        if(this.lineItems == null){
          this.lineItems = new HashSet<>();
        }
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }
}
