package com.classpath.orderserviceapi.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error handleIllegalArgumentException(IllegalArgumentException exception){
        log.error("Handling invalid order id :: {}", exception.getMessage());
        return new Error(100, exception.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Set<String> handleInvalidDataException(MethodArgumentNotValidException exception){
        log.error("Handling invalid order passed :: {}", exception.getMessage());
        List<ObjectError> allErrors = exception.getAllErrors();
        return allErrors
                .stream()
                .map(objectError -> objectError.getDefaultMessage())
                .collect(Collectors.toSet());
    }
}

@AllArgsConstructor
@Getter
class Error {
    private int code;
    private String message;
}
