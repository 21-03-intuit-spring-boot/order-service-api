package com.classpath.orderserviceapi.service;

import com.classpath.orderserviceapi.model.Order;
import com.classpath.orderserviceapi.repository.OrderRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderService {
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Order saveOrder(Order order){
        return this.orderRepository.save(order);
    }

    public Map<String, Object> fetchAllOrders(int page, int size, String direction, String property){
        Sort.Direction sortDirection = direction.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        PageRequest pageRequest = PageRequest.of(page, size, sortDirection, property);
        Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);
        long totalRecords = pageResponse.getTotalElements();
        int totalPages = pageResponse.getTotalPages();
        int number = pageResponse.getNumber();
        int recordsPerPage = pageResponse.getSize();
        List<Order> data = pageResponse.getContent();

        LinkedHashMap<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("total-records", totalRecords);
        responseMap.put("page", number);
        responseMap.put("size", recordsPerPage);
        responseMap.put("total-pages", totalPages);
        responseMap.put("data", data);
        return responseMap;
    }

    public Order fetchOrderById(long orderId){
        // user?.name
        return this.orderRepository.findById(orderId)
                                    .orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }

    public List<Order> fetchOrderByPriceRange(double min, double max){
        return this.orderRepository.findByPriceBetween(min, max);
    }
}
